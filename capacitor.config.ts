import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'sveltenative-test',
  webDir: 'build',
  bundledWebRuntime: false,
  server: {
      //url: "http://10.0.2.2:3000",
      url: "http://127.0.0.1:5173",
      cleartext: true,
  },
};

export default config;
